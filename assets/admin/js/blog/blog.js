$(document).ready(function () {

    const base_url = document.getElementById('base_url').innerHTML;

    $('.blog_title').richText();  // initialize the reach text for post title

    $('.blog_content').richText();   // initialize the reach text for post content

    $('#blog_heading').next().find('.richText-editor').css({'height': '80px', 'overflow': 'auto'});  // fix the height of post title reach text box
   
    $('.richText-dropdown').css('z-index', '999');

      
    $('#upload, #update').click(function () {
        const title = $('.blog_title').val();
        const content = $('.blog_content').val();
        const status = $('#blog_status').val();
        const id = $(this).prop('id')
       
        const data = new FormData();
        data.append('title', title);
        data.append('content', content);
        data.append('status', status);

        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              const resp = JSON.parse(this.responseText)

              if(resp.msg === 'success') {
                if(id == 'update')
                  location.replace(base_url+'admin/updateBlog')
                else
                  location.reload();
              }
              else {
                if(id == 'update')
                  alert('Problem in updating! Try again')
                else
                  alert('Problem in uploading! Try again')
              }
          }
        };
        if(id == 'upload')
          xhttp.open("POST", base_url+"admin/uploadBlog", true);
        else
          xhttp.open("POST", base_url+`admin/updateBlogById/${$('#updating_id').val()}`, true);

        xhttp.send(data);
    });


});
