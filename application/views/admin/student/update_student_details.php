<div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><i class="fas fa-user-graduate"></i> Students Details</h1>

    <div id="up" class="card" style="border: 1px solid orange;">
        <div class="card-header text-dark font-weight-bold" style="border-top: 3px solid orange;">
            <i class="fas fa-comment-dots"></i> Update Student
        </div>

        <form action="<?=base_url()?>admin/update-student/<?=$data[0]['id']?>" method="POST">
            <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="label-black font-weight-bold">Student Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-user" value="<?=$data[0]['sname']?>" id="sname" name="sname" placeholder="Student Name..." required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="label-black font-weight-bold">Roll No <span class="text-danger">*</span></label>
                                <input type="number" class="form-control form-control-user" value="<?=$data[0]['roll_no']?>" id="roll_no" name="roll_no" placeholder="Roll No..." required>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="label-black font-weight-bold">DOB <span class="text-danger">*</span></label>
                                <input type="date" class="form-control form-control-user" value="<?=$data[0]['dob']?>" id="dob" name="dob" placeholder="DOB..." required>
                            </div>
                        </div>

                    </div>
            
            </div>

            <div class="card-footer text-right">
                <input type="submit" class="btn btn-primary" value="Submit" />
            </div>
        </form>
    </div>

</div>