 <!-- Begin Page Content -->
 <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><i class="fas fa-user-graduate"></i> Result</h1>

    <div class="card" style="border: 1px solid orange;">

            <div class="card-header text-dark font-weight-bold" style="border-top: 3px solid orange;">
                <i class="fas fa-comment-dots"></i> Compose New Result
            </div>

            <form action="<?=base_url()?>admin/save-result" method="POST" enctype="multipart/form-data">
                <div class="card-body">
                
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <div class="form-group">
                                    <label class="label-black font-weight-bold">Roll No <span class="text-danger">*</span></label>
                                    <input type="number" class="form-control form-control-user" id="rollno" name="rollno" placeholder="Roll No..." required autofocus>
                                </div>

                                <div class="form-group">
                                    <label class="label-black font-weight-bold">Upload Result | <span class="text-danger">Only PDF *</span></label>
                                    <input type="file" class="form-control form-control-user" id="result_pdf" name="result_pdf" required>
                                </div>

                            </div>

                        </div>
                
                </div>

                <div class="card-footer text-right">
                    <input type="submit" class="btn btn-primary" value="Submit" />
                </div>
            </form>
    </div>


<!-- Show Uploded data -->

    <div class="card mt-4">
        <div class="card-header text-dark font-weight-bold" style="border-top: 3px solid orange;">
            <div class="float-left"><i class="fas fa-comment-dots"></i> Uploaded Result</div>
        </div>
        <div class="card-body">
        <?php if(!empty($data)) :?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">S.N.</th>
                        <th scope="col">Student Name</th>
                        <th scope="col">Roll No</th>
                        <th scope="col">Result PDF</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count=1;  foreach ($data as $d): ?>
                        <tr>
                            <td> <?=$count?> </td>
                            <td> <?=$d['sname']?> </td>
                            <td> <?=$d['roll_no']?> </td>
                            <td style="word-break: break-all;"> 
                                <a href="<?=base_url()."assets/uploads/result/".$d['result_pdf']?>" target="_blank">
                                    <?=$d['result_pdf']?>
                                </a>
                            </td>
                            <td> <?=date("d-m-Y h:i:s", strtotime($d['created_at']))?> </td>
                            <td> 
                                <a href="<?=base_url()?>admin/delete-result/<?=$d['id']?>/<?=$d['result_pdf']?>"  class="btn btn-link text-danger">Delete</a>
                            </td>
                        </tr>
                        <?php $count++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else :?>
                <div class="alert alert-danger" role="alert">
                    No Data Found !
                </div>
        <?php endif;?>
        </div>
    </div>

</div>
<!-- /.container-fluid -->