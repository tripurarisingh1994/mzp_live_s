 <!-- Begin Page Content -->
 <div class="container-fluid">

<!-- Page Heading -->
<h1 class="h3 mb-4 text-gray-800"><i class="fas fa-bullhorn"></i> Affiliation</h1>

    <div class="card" style="border: 1px solid orange;">
        <div class="card-header text-dark font-weight-bold" style="border-top: 3px solid orange;">
            <i class="fas fa-comment-dots"></i> Compose New Affiliation
        </div>

        <form action="<?=base_url()?>admin/save-aff" method="POST" enctype="multipart/form-data">
            <div class="card-body">
            
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-black font-weight-bold">College Name <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-user" id="coll_name" name="coll_name" placeholder="College Name..." required autofocus>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-black font-weight-bold">College Code <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-user" id="coll_code" name="coll_code" placeholder="College Code..." required>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <div class="form-group">
                                <label class="label-black font-weight-bold">Affiliation No <span class="text-danger">*</span></label>
                                <input type="text" class="form-control form-control-user" id="affno" name="affno" placeholder="Affiliation No..." required>
                            </div>
                        </div>

                        <div class="col-md-3">
                                <div class="form-group">
                                <label class="label-black font-weight-bold">Upload Affiliation | <span class="text-danger">Only PDF *</span></label>
                                <input type="file" class="form-control form-control-user" id="aff_pdf" name="aff_pdf" required>
                            </div>
                        </div>

                    </div>
            
            </div>

            <div class="card-footer text-right">
                <input type="submit" class="btn btn-primary" value="Submit" />
            </div>
        </form>
    </div>



    <!-- Show Uploded data -->

    <div class="card mt-4">
        <div class="card-header text-dark font-weight-bold" style="border-top: 3px solid orange;">
            <div class="float-left"><i class="fas fa-comment-dots"></i> Uploaded Affiliation</div>
        </div>
        <div class="card-body">
        <?php if(!empty($data)) :?>
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">S.N.</th>
                        <th scope="col">College Name</th>
                        <th scope="col">College Code</th>
                        <th scope="col">Affiliation No</th>
                        <th scope="col">Affiliation Pdf</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $count=1;  foreach ($data as $d): ?>
                        <tr>
                            <td> <?=$count?> </td>
                            <td> <?=$d['coll_name']?> </td>
                            <td> <?=$d['coll_code']?> </td>
                            <td> <?=$d['aff_no']?> </td>
                            <td style="word-break: break-all;"> 
                                <a href="<?=base_url()."assets/uploads/aff/".$d['aff_pdf']?>" target="_blank">
                                    <?=$d['aff_pdf']?>
                                </a>
                            </td>
                            <td> <?=date("d-m-Y h:i:s", strtotime($d['created_at']))?> </td>
                            <td> 
                                <a href="<?=base_url()?>admin/delete-aff/<?=$d['id']?>/<?=$d['aff_pdf']?>"  class="btn btn-link text-danger">Delete</a>
                            </td>
                        </tr>
                        <?php $count++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        <?php else :?>
                <div class="alert alert-danger" role="alert">
                    No Data Found !
                </div>
        <?php endif;?>
        </div>
    </div>

    

</div>
<!-- /.container-fluid -->