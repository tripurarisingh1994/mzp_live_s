<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
   require APPPATH . '/libraries/REST_Controller.php';
   use Restserver\Libraries\REST_Controller;
     
class Signup extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
 
        $this->load->model('auth_model', 'authM');
     }


     public function index_post()
     {
        $salt = "!fG8kZnrdfkd+4s!K$%MeSMCD_usZ#";
        $password = hash('sha512', $salt.$this->input->post('email').$this->input->post('password'));

        $chk = $this->authM->create_user($this->input->post('name'), $this->input->post('email'), $password);
        
        if($chk == 1)
        {
            $status = parent::HTTP_OK;
            $response = ['status' => $status, 'msg' => 'user created successfully', 'chk' => TRUE];
            $this->response($response, $status);   
         }
         else
         {
            $status = parent::HTTP_BAD_REQUEST;
            $response = ['status' => $status, 'msg' => 'Something went worng! Please try again', 'chk' => FALSE];
            $this->response($response, $status);
         }
     }

}