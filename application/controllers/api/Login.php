<?php
   defined('BASEPATH') OR exit('No direct script access allowed');
   
   require APPPATH . '/libraries/REST_Controller.php';
   use Restserver\Libraries\REST_Controller;
     
class Login extends REST_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(['jwt', 'authorization']); 
 
        $this->load->model('auth_model', 'authM');
     }

     public function index_get()
     {
        // $status = parent::HTTP_OK;
        // $response = ['status' => $status, 'msg' => 'successful'];
        // $this->response($response, $status);
        
        // $headers = $this->input->request_headers();
        // $token = $headers['Authorization'];

        // try {
        //     $data = AUTHORIZATION::validateToken($token);

        //     if ($data === false) {

        //             $status = parent::HTTP_UNAUTHORIZED;
        //             $response = ['status' => $status, 'msg' => 'Unauthorized Access!'];
        //             $this->response($response, $status);
        //             exit();

        //     } 
        //     else {
        //             $status = parent::HTTP_OK;
        //             $response = ['status' => $status, 'msg' => 'successful'];
        //             $this->response($response, $status);
        //     }
        // } 
        // catch (Exception $e) {

        //         $status = parent::HTTP_UNAUTHORIZED;
        //         $response = ['status' => $status, 'msg' => 'Unauthorized Access! '];
        //         $this->response($response, $status);
        // }

     }

    public function index_post()
	{
        $salt = "!fG8kZnrdfkd+4s!K$%MeSMCD_usZ#";
        $password = hash('sha512', $salt.$this->input->post('email').$this->input->post('password'));
        
        $res = $this->authM->check_email($this->input->post('email'));      // Email Check 

        if($res->num_rows() === 1) {
            
            $res1 = $this->authM->do_login($this->input->post('email'), $password);             // Do Login

            if($res1->num_rows() === 1)
            {
                $row = $res1->result_array();
                $data = array(
                    'id' => $row[0]['id'],
                    'name' => $row[0]['name'],
                    'email' => $row[0]['email'],
                );
                $token = AUTHORIZATION::generateToken($data);

                $status = parent::HTTP_OK;
                $response = ['status' => $status, 'token' => $token];
                $this->response($response, $status);
            }
            else {
                $status = parent::HTTP_BAD_REQUEST;
                $response = ['status' => $status, 'msg' => 'Email or Password is worng!'];
                $this->response($response, $status);
            }
        }
        else {
            $status = parent::HTTP_NOT_FOUND;
            $response = ['status' => $status, 'msg' => 'This email does not exist.'];
            $this->response($response, $status);
        }
    }
    

}