<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

    function __construct() 
    {
        parent::__construct();
        if (! $this->session->userdata('logged_in'))
        { 
            redirect('admin');
        }
        $this->load->model('admin/administration_model', 'af');
    }

    public function affiliation() 
    {
        $data['css'] = "";
        $data['js'] = "";
        $data['data'] = $this->get_aff();
        $this->load->view('admin/common/header', $data);
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/common/navbar');
        $this->load->view('admin/administration/affiliation');
        $this->load->view('admin/common/footer', $data);
    }


    public function saveAff()
    {
            $coll_name = $this->input->post('coll_name');
            $coll_code = $this->input->post('coll_code');
            $affno = $this->input->post('affno');
        
            $aff = $this->aff_upload();

            $res = '';

            if ($aff) {

                $aff_fname = $aff['res']['file_name'];
                $res = $this->af->create_aff($coll_name, $coll_code, $affno, $aff_fname);
            }
            else 
            {
                echo '<script>alert("Error in uploading, Please choose the correct format of the file.");window.location.href="'.base_url().'admin/affiliation";</script>';
            }

            if ($res == 1) {
                echo '<script>alert("Data Added Successfully");window.location.href="'.base_url().'admin/affiliation";</script>';
            }
            else {
                    echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/affiliation";</script>';
            }
    }


    function aff_upload()
    {
        if (!is_dir('./assets/uploads/aff/')) 
        {
            mkdir('./assets/uploads/aff/', 0777, TRUE);
        }

        $config['upload_path']   = './assets/uploads/aff/';
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = 5000;
        $config['overwrite']     = FALSE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

       if ( ! $this->upload->do_upload('aff_pdf'))
        {
            return array('error' => $this->upload->display_errors());
        }
         else
        {
            return array('res' => $this->upload->data());
        }
    }


    function get_aff($id=NULL)
     {
         $res = $this->af->read_aff($id);
 
         if($res->num_rows() > 0) 
             return $res->result_array();
         else 
             return array();
     }


     // delete admit card
      public function deleteAff($id, $pdf)
      {
          $this->load->helper('path');
          $this->load->helper('file');
          $path = "assets/uploads/aff/".$pdf;
          $_path = set_realpath($path);
 
          $cnf = unlink($_path);

          if($cnf == 1)
          {
             $res = $this->af->delete_aff($id);
 
             if ($res == 1) {
                 echo '<script>alert("Data Deleted Successfully");window.location.href="'.base_url().'admin/affiliation";</script>';
             }
             else {
                 echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/affiliation";</script>';
             }
          }
          else {
             echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/affiliation";</script>';
          }
      }


}