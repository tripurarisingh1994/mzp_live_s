<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller 
{

     function __construct()
    {
        parent::__construct();
        $this->load->model('admin/auth_model', 'authM');
    }

    public function index()
    {
        $this->load->view('admin/auth/login');      // login page redirect
    }

    public function login() 
    {
        if ($this->session->userdata('logged_in'))
        { 
            redirect('admin/dashboard');
        }

        $email    = $this->input->post('email');
        $password = hash('sha512', $this->input->post('pass'));

        $res = $this->authM->doLogin($email, $password);

        if( $res->num_rows() == 1 ) 
        {
            $row = $res->result_array(); // $res convert into associative array
            $this->authM->last_login($row[0]['id']);

            $userdata = array(
                'id'        => $row[0]['id'],
                'email'     => $row[0]['email'],
                'logged_in' => TRUE
            );
        
            $this->session->set_userdata($userdata);
            redirect('admin/dashboard');
        }
        else 
        {
            echo "<script>alert('Incorrect Email or Password !'); window.history.back();</script>";
        }
    }


    public function logout() 
    {
        $this->session->sess_destroy();
        redirect('admin');
    }

}