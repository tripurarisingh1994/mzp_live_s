<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    function __construct() 
    {
        parent::__construct();
        if (! $this->session->userdata('logged_in'))
        { 
            redirect('admin');
        }
    }

    public function index() 
    {
        $data['css'] = "";
        $data['js'] = "";
        $this->load->view('admin/common/header', $data);
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/common/navbar');
        $this->load->view('admin/dashboard/dashboard');
        $this->load->view('admin/common/footer', $data);
    }

}