<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

    function __construct() 
    {
        parent::__construct();
        if (! $this->session->userdata('logged_in'))
        { 
            redirect('admin');
        }

        $this->load->model('admin/student_model', 'stu');
    }

    // Loading Student Details and Adding its Details
    public function index()
    {
        $data['css'] = "";
        $data['js'] = "";
        $data['data'] = $this->get_student();
        $this->load->view('admin/common/header', $data);
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/common/navbar');
        $this->load->view('admin/student/student_details');
        $this->load->view('admin/common/footer', $data);
    }

    public function admit_card() 
    {
        $data['css'] = "";
        $data['js'] = "";
        $data['data'] = $this->get_ac();
        $this->load->view('admin/common/header', $data);
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/common/navbar');
        $this->load->view('admin/student/admit_card');
        $this->load->view('admin/common/footer', $data);
    }

    public function result() 
    {
        $data['css'] = "";
        $data['js'] = "";
        $data['data'] = $this->get_result();
        $this->load->view('admin/common/header', $data);
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/common/navbar');
        $this->load->view('admin/student/result');
        $this->load->view('admin/common/footer', $data);
    }

    public function saveResult()
    {
        $roll_no = $this->input->post('rollno');
        
        $chk = $this->stu->check_stu_by_roll($roll_no);
        
        if($chk->num_rows()>0)
        {
            $result = $this->result_upload();

            $_d = $chk->result_array();

                $res = '';

                if ($result) {

                    $result_fname = $result['res']['file_name'];
                    $res = $this->stu->create_result( $_d[0]['id'], $result_fname);
                }
                else 
                {
                    echo '<script>alert("Error in uploading, Please choose the correct format of the file.");window.location.href="'.base_url().'admin/result";</script>';
                    // redirect('admin/result');    
                }

            if ($res == 1) {
                echo '<script>alert("Data Added Successfully");window.location.href="'.base_url().'admin/result";</script>';
                //    redirect('admin/result');
            }
            else {
                    echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/result";</script>';
                    //  redirect('admin/result'); 
            }
        }
        else {
            echo '<script>alert("This is student is not there, Please add it");window.location.href="'.base_url().'admin/result";</script>';
        }
    }


    function result_upload()
    {
        if (!is_dir('./assets/uploads/result/')) 
        {
            mkdir('./assets/uploads/result/', 0777, TRUE);
        }

        $config['upload_path']   = './assets/uploads/result/';
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = 5000;
        $config['overwrite']     = FALSE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

       if ( ! $this->upload->do_upload('result_pdf'))
        {
            return array('error' => $this->upload->display_errors());
        }
         else
        {
            return array('res' => $this->upload->data());
        }
    }


     // Get the result data using modal
     function get_result($id=NULL)
     {
         $res = $this->stu->read_result($id);
 
         if($res->num_rows() > 0) 
             return $res->result_array();
         else 
             return array();
     }

// Delete pdf file
     public function deleteResult($id, $pdf)
     {
         $this->load->helper('path');
         $this->load->helper('file');
         $path = "assets/uploads/result/".$pdf;
         $_path = set_realpath($path);

         $cnf = unlink($_path);

         if($cnf == 1)
         {
            $res = $this->stu->delete_result($id);

            if ($res == 1) {
                echo '<script>alert("Data Deleted Successfully");window.location.href="'.base_url().'admin/result";</script>';
            }
            else {
                echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/result";</script>';
            }
         }
         else {
            echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/result";</script>';
         }
     }


    //  saving the student details

    public function saveStudent()
    {
        $sname = $this->input->post('sname');
        $roll_no = $this->input->post('roll_no');
        $dob = $this->input->post('dob');

        $res = $this->stu->create_student($sname, $roll_no, $dob);

        if ($res == 1) {
            echo '<script>alert("Data Added Successfully");window.location.href="'.base_url().'admin/student";</script>';
         //    redirect('admin/result');
        }
        else {
              echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/student";</script>';
             //  redirect('admin/result'); 
        }

    }


    // Get the student data using modal
    function get_student($id=NULL)
     {
         $res = $this->stu->read_students($id);
 
         if($res->num_rows() > 0) 
             return $res->result_array();
         else 
             return array();
     }

     // Delete the student

     function deleteStudent($id)
     {
        $res = $this->stu->delete_student($id);

        if ($res == 1) {
            echo '<script>alert("Data Deleted Successfully");window.location.href="'.base_url().'admin/student";</script>';
         //    redirect('admin/result');
        }
        else {
              echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/student";</script>';
             //  redirect('admin/result'); 
        }
     }

     public function view_updateStudent($id)
     {
        $data['css'] = "";
        $data['js'] = "";
        $data['data'] = $this->get_student($id);
        $this->load->view('admin/common/header', $data);
        $this->load->view('admin/common/sidebar');
        $this->load->view('admin/common/navbar');
        $this->load->view('admin/student/update_student_details');
        $this->load->view('admin/common/footer', $data);
     }

     public function updateStudent($id)
     {
        $sname = $this->input->post('sname');
        $roll_no = $this->input->post('roll_no');
        $dob = $this->input->post('dob');

        $res = $this->stu->update_student($id, $sname, $roll_no, $dob);

        if ($res == 1) {
            echo '<script>alert("Data Updated Successfully");window.location.href="'.base_url().'admin/student";</script>';
         //    redirect('admin/result');
        }
        else {
              echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/student";</script>';
             //  redirect('admin/result'); 
        }
     }



     /**
      * Admit Card Section ***************************/

      public function saveAdmitCard()
      {
          $roll_no = $this->input->post('rollno');
          
          $chk = $this->stu->check_stu_by_roll($roll_no);
          
          if($chk->num_rows()>0)
          {
              $ac = $this->ac_upload();         // Admit Card Upload
  
              $_d = $chk->result_array();
  
                  $res = '';
  
                  if ($ac) {

                      $ac_fname = $ac['res']['file_name'];
                      $res = $this->stu->create_ac( $_d[0]['id'], $ac_fname);
                  }
                  else 
                  {
                      echo '<script>alert("Error in uploading, Please choose the correct format of the file.");window.location.href="'.base_url().'admin/admit-card";</script>';
                  }
  
              if ($res == 1) {
                  echo '<script>alert("Data Added Successfully");window.location.href="'.base_url().'admin/admit-card";</script>';
              }
              else {
                      echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/admit-card";</script>';
              }
          }
          else {
              echo '<script>alert("This is student is not there, Please add it");window.location.href="'.base_url().'admin/admit-card";</script>';
          }
      }



      function ac_upload()
      {
        if (!is_dir('./assets/uploads/ac/')) 
        {
            mkdir('./assets/uploads/ac/', 0777, TRUE);
        }

        $config['upload_path']   = './assets/uploads/ac/';
        $config['allowed_types'] = 'pdf';
        $config['max_size']      = 5000;
        $config['overwrite']     = FALSE;

        $this->load->library('upload', $config);
        $this->upload->initialize($config);

       if ( ! $this->upload->do_upload('ac_pdf'))
        {
            return array('error' => $this->upload->display_errors());
        }
         else
        {
            return array('res' => $this->upload->data());
        }
      }

      function get_ac($id=NULL)
      {
        $res = $this->stu->read_ac($id);
 
        if($res->num_rows() > 0) 
            return $res->result_array();
        else 
            return array();
      }

// delete admit card
      public function deleteAdmitCard($id, $pdf)
      {
          $this->load->helper('path');
          $this->load->helper('file');
          $path = "assets/uploads/ac/".$pdf;
          $_path = set_realpath($path);
 
          $cnf = unlink($_path);
 
          if($cnf == 1)
          {
             $res = $this->stu->delete_ac($id);
 
             if ($res == 1) {
                 echo '<script>alert("Data Deleted Successfully");window.location.href="'.base_url().'admin/admit-card";</script>';
             }
             else {
                 echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/admit-card";</script>';
             }
          }
          else {
             echo '<script>alert("Something went wrong, Try again!");window.location.href="'.base_url().'admin/admit-card";</script>';
          }
      }

}