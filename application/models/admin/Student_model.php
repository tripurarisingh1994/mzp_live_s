<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model 
{

    function check_stu_by_roll($roll_no)
    {
        return $this->db->where('roll_no', $roll_no)
                            ->get('students');
    }

    function create_result($id, $result_fname) 
    {
        $data = array(
            's_id' => $id,
            'result_pdf' => $result_fname,
            'created_at' => date('Y-m-d H:i:s'),
        );

        return $this->db->insert('result', $data);
    }

    function read_result($id) 
    {
        if($id)
        {
            return $this->db->where(array('r.id' => $id))
                            ->select('s.sname, s.roll_no, r.id, r.result_pdf, r.created_at',false)
                            ->from('result as r')
                            ->join('students as s','r.s_id=s.id', 'left')
                            ->get();
        }
        else
        {
            return $this->db->from('result as r')
                            ->select('s.sname, s.roll_no, r.id, r.result_pdf, r.created_at',false)
                            ->join('students as s','r.s_id=s.id', 'left')
                            ->get();
        }
    }

    function delete_result($id) {
        return $this->db->delete('result', array('id' => $id));
    }


    function create_student($sname, $roll_no, $dob)
    {
        $data=array(
            'sname' => $sname,
            'roll_no' => $roll_no,
            'dob' => $dob,
            'created_at' => date('Y-m-d H:i:s'),
        );

        return $this->db->insert('students', $data);
    }


    function read_students($id) {
        if($id)
        {
            return $this->db->where('id', $id)
                            ->get('students');
        }
        else
            return $this->db->get('students');
    }

    function delete_student($id) {
       return $this->db->delete('students', array('id' => $id));
    }

    function update_student($id, $sname, $roll_no, $dob)
    {
        $data = array(
            'sname' => $sname,
            'roll_no' => $roll_no,
            'dob' => $dob,
            'updated_at' => date('Y-m-d H:i:s'),
        );
    
        return $this->db->where('id', $id)
                        ->update('students', $data);
    }





    /***
     ******************** Admit Card Section ************************/


    function create_ac($id, $ac_fname)
    {
        $data = array(
            's_id' => $id,
            'ac_pdf' => $ac_fname,
            'created_at' => date('Y-m-d H:i:s'),
        );

        return $this->db->insert('admit_card', $data);
    }

    function read_ac($id)
    {
        if($id)
        {
            return $this->db->where(array('ac.id' => $id))
                            ->select('s.sname, s.roll_no, ac.id, ac.ac_pdf, ac.created_at',false)
                            ->from('admit_card as ac')
                            ->join('students as s','ac.s_id=s.id', 'left')
                            ->get();
        }
        else
        {
            return $this->db->from('admit_card as ac')
                            ->select('s.sname, s.roll_no, ac.id, ac.ac_pdf, ac.created_at',false)
                            ->join('students as s','ac.s_id=s.id', 'left')
                            ->get();
        }
    }


    function delete_ac($id)
    {
        return $this->db->delete('admit_card', array('id' => $id));   
    }
}