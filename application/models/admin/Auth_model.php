<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model 
{
   function doLogin($email, $password){

        $this->db->where('al.email', $email);
        $this->db->where('al.password', $password);
        $this->db->where('al.active', 1);
        $this->db->from('admin_login as al');

        return  $this->db->get();
   }

    function last_login($id) {
        $data = array(
            'last_login' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        $this->db->update('admin_login', $data);
    }
}