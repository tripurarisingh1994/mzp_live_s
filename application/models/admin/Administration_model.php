<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Administration_model extends CI_Model 
{
    function create_aff($coll_name, $coll_code, $affno, $aff_fname) 
    {
        $data = array(
            'coll_name' => $coll_name,
            'coll_code' => $coll_code,
            'aff_no' => $affno,
            'aff_pdf' => $aff_fname,
            'created_at' => date('Y-m-d H:i:s'),
        );

        return $this->db->insert('affiliation', $data);
    }


    function read_aff($id)
    {
        if($id)
        {
            return $this->db->where('id', $id)
                            ->get('affiliation');
        }
        else
            return $this->db->get('affiliation');
    }

    

    function delete_aff($id)
    {
        return $this->db->delete('affiliation', array('id' => $id));
    }
}