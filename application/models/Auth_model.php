<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model 
{
  // C -> Create
  // U -> Update
  // R -> Read
  // D -> Delete

  function check_email($email)
  {
    return $this->db->get_where('users', array('email' => $email));
  }

  function do_login($email, $password)
  {
    return $this->db->where(array('email' => $email, 'password' => $password, 'is_active' => 1))
                    ->get('users');
  }

  function create_user($name, $email, $password)
  {
    $data = array(
      'name' => $name,
      'email' => $email,
      'password' => $password,
      'is_active' => 1,
      'created_at' => date('Y-m-d H:i:s')
    );

    return $this->db->insert('users', $data);
    
  }
}