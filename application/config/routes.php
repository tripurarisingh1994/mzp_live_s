<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;


//Admin Controller
$route['admin'] = 'admin/auth';
$route['admin/login'] = 'admin/auth/login';      // redirect to the login method
$route['admin/logout'] = 'admin/auth/logout';   // reditect to the logout method

$route['admin/dashboard'] = 'admin/dashboard';

$route['admin/student'] = 'admin/student';

$route['admin/result'] = 'admin/student/result';
$route['admin/delete-result/(:num)/(:any)'] = 'admin/student/deleteResult/$1/$2';
$route['admin/save-result'] = 'admin/student/saveResult';



$route['admin/save-student'] = 'admin/student/saveStudent';
$route['admin/delete-student/(:num)'] = 'admin/student/deleteStudent/$1';
$route['admin/v_update-student/(:num)'] = 'admin/student/view_updateStudent/$1';
$route['admin/update-student/(:num)'] = 'admin/student/updateStudent/$1';



$route['admin/admit-card'] = 'admin/student/admit_card';
$route['admin/save-admit-card'] = 'admin/student/saveAdmitCard';
$route['admin/delete-admit-card/(:num)/(:any)'] = 'admin/student/deleteAdmitCard/$1/$2';






$route['admin/affiliation'] = 'admin/administration/affiliation';
$route['admin/save-aff'] = 'admin/administration/saveAff';
$route['admin/delete-aff/(:num)/(:any)'] = 'admin/administration/deleteAff/$1/$2';




// Flash
// $route['success-flash/(:any)'] = 'myFlash/success/$1';
// $route['error-flash'] = 'myFlash/error';
// $route['warning-flash'] = 'myFlash/warning';
// $route['info-flash'] = 'myFlash/info';



//client side Controller
// $route['result'] = 'result';
// $route['get-result'] = 'result/getResult';

// $route['admit-card'] = 'admitCard';
// $route['get-ac'] = 'admitCard/getAdmitCard';

// $route['affiliation'] = 'affiliation';
// $route['get-af'] = 'affiliation/getAff';


